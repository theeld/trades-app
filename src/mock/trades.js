export const trades = [
	{ id: 1, counterpartyId: 1, direction: "Buy", product: "Sugar", quantity: 100, price: 400.50, date: "2019-01-31T08:00:10.218Z" },
	{ id: 2, counterpartyId: 2, direction: "Sell", product: "Sugar", quantity: 100, price: 450.10, date: "2019-03-31T08:00:10.218Z" }
]