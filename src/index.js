import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Form from './components/Form/Form';
import TradeList from './components/TradeList/TradeList';
import Page404 from './components/Page404/Page404'
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Switch, Route } from 'react-router-dom'

ReactDOM.render(
	<BrowserRouter>
		<Switch>
			<Route path="/" exact={true} component={TradeList} />
			<Route path="/trade/create" exact={true} component={Form} />
			<Route path="/trade/edit/:id" exact={true} component={Form} />
			<Route path='*' component={Page404} />
		</Switch>
	</ BrowserRouter>,
	document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
