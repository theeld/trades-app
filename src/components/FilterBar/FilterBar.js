import React from 'react'
import ApiService from './../../services/api'
import './FilterBar.css'

class FilterBar extends React.Component {
	_isMounted = false
	constructor(props) {
		super(props)
		this.state = {
			counterparties: []
		}
		this.handleSubmit = this.handleSubmit.bind(this)
	}

	componentDidMount() {
		this._isMounted = true
		this.fetchCounterparties()
	}

	componentWillUnmount() {
		this._isMounted = false;
	}

	fetchCounterparties = async () => {
		const response = await ApiService.getCounterparties()
		if (this._isMounted) this.setState({ counterparties: response })
	}

	handleSubmit(event) {
		this.props.onFilterSelect(parseInt(event.target.counterparty.value))
		event.preventDefault()
	}

	render() {
		const options = []
		this.state.counterparties.forEach((counterparty) => {
			options.push(
				<option key={counterparty.id} value={counterparty.id}>{counterparty.name}</option>
			)
		})
		return (
			<div className="FilterBar">
				<div className="FilterBar-header">
					<p>Search</p>
				</div>
				<div className="FilterBar-body">
					<form onSubmit={this.handleSubmit}>
						<label>
							Counterparty
          <select className="FilterBar-select" name="counterparty" defaultValue={this.props.initialSelectedValue}>
								<option value="0">Select</option>
								{options}
							</select>
						</label>
						<input className="FilterBar-button" type="submit" value="Search"></input>
					</form>
				</div>
			</div>
		)
	}
}
export default FilterBar