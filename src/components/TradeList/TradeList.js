import React from 'react'
import FilterBar from '../FilterBar/FilterBar'
import TradeTable from '../TradeTable/TradeTable'
import { Link } from 'react-router-dom'
import './TradeList.css'

class MainComponent extends React.Component {

	constructor(props) {
		super(props)
		this.state = {
			filterOption: 0
		}
		this.handleFilterChange = this.handleFilterChange.bind(this)
	}

	handleFilterChange(filterOption) {
		this.setState({
			filterOption: filterOption
		})
	}

	render() {
		const filter = this.state.filterOption
		return (
			<div className="TradeList">
				<h1 className="TradeList-title">Trades</h1>
				<FilterBar
					initialSelectedValue='0'
					onFilterSelect={this.handleFilterChange}
				/>
				<TradeTable
					filterCounterparty={filter}
				/>
				<Link to="/trade/create"><button className="TradeList-button">Create a new trade</button></Link>
			</div>
		)
	}
}
export default MainComponent