import React from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom'
import './TradeRow.css'

function TableRow(props) {
		const trade = props.trade
		function deleteDialog(id) {
			if (window.confirm(`Do you really want to delete this trade #${id}?`)) {
				props.onTradeDelete(id)
			}
		}
    return (
      <tr className="TradeRow">
        <td>{trade.id}</td>
        <td>{trade.counterpartyName}</td>
				<td>{trade.direction}</td>
				<td>{trade.product}</td>
				<td>{trade.quantity}</td>
				<td>{trade.price}</td>
				<td>{moment(trade.date).format('DD/MM/YYYY')}</td>
				<td>
					<Link to={`/trade/edit/${trade.id}`}><button className="TradeRow-button">Edit</button></Link>
					<button className="TradeRow-button" onClick={(e) => deleteDialog(trade.id)}>Remove</button>
				</td>
      </tr>
    )
}
export default TableRow