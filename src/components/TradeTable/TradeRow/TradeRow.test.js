import React from 'react';
import ReactDOM from 'react-dom';
import TradeRow from './TradeRow';
import { BrowserRouter } from 'react-router-dom'

const trade = { id: 1, counterpartyId: 1, direction: "Buy", product: "Sugar", quantity: 100, price: 400.50, date: "2019-01-31T08:00:10.218Z" }
function deleteTrade(event) {
}

describe('TradeRow', () => {
	it('renders row without crashing', () => {
		const tbody = document.createElement('tbody');
		ReactDOM.render(<BrowserRouter><TradeRow trade={trade} key={trade.id} onTradeDelete={deleteTrade}/></BrowserRouter>, tbody);
		ReactDOM.unmountComponentAtNode(tbody);
	})
})