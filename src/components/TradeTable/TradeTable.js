import React from 'react'
import TradeRow from './TradeRow/TradeRow'
import ApiService from './../../services/api'
import './TradeTable.css'

class TradeTable extends React.Component {
	_isMounted = false
	constructor(props) {
		super(props);
		this.state = {
			counterparties: [],
			trades: [],
			loading: true
		}
		this.deleteTrade = this.deleteTrade.bind(this)
	}

	componentDidMount() {
		this._isMounted = true
		this.fetchCounterparties()
		this.fetchTrades()
	}

	componentWillUnmount() {
		this._isMounted = false
	}

	fetchCounterparties = async () => {
		const response = await ApiService.getCounterparties()
		if (this._isMounted) this.setState({ counterparties: response })
	}

	fetchTrades = async () => {
		const response = await ApiService.getTrades()
		this.setState({ loading: false })
		if (this._isMounted) this.setState({ trades: response })
	}

	deleteTrade(id) {
		ApiService.deleteTrade(id).then(trades => this.setState({ trades: trades }))
	}

	render() {
		const trades = this.state.trades
		const filter = this.props.filterCounterparty
		const rows = []

		trades.forEach((trade) => {
			if (filter && trade.counterpartyId !== filter) {
				return
			}
			const counterpartyName = this.state.counterparties.find(cnp => cnp.id === trade.counterpartyId).name
			trade = { ...trade, counterpartyName: counterpartyName }
			rows.push(
				<TradeRow
					trade={trade}
					key={trade.id}
					onTradeDelete={this.deleteTrade}
				/>
			)
		})

		return (
			<div className="TradeTable">
				{this.state.loading && (
					<p>Loading...</p>
				)}
				{!this.state.loading && (
					<table>
						<thead>
							<tr>
								<th>#</th>
								<th>Counterparty</th>
								<th>Direction</th>
								<th>Product</th>
								<th>Quantity</th>
								<th>Price</th>
								<th>Date</th>
								<th></th>
							</tr>
						</thead>
						<tbody>{rows}</tbody>
					</table>
				)}
			</div>
		);
	}
}
export default TradeTable