import React from 'react'

function Page404(props) {
	const divStyle = {
		padding: '2rem 4rem'
	}
	return (
		<div style={divStyle}>
			<h1>404</h1>
			<p>This is not the page you are looking for :/</p>
		</div>
	)
}
export default Page404