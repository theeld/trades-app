import React from 'react'
import ApiService from './../../services/api'
import moment from 'moment'
import './Form.css'

class Form extends React.Component {
	_isMounted = false
	loading = true
	constructor(props) {
		super(props)
		this.state = {
			counterparties: [],
			counterpartyId: '',
			direction: 'Buy',
			product: '',
			quantity: '',
			price: '',
			date: '',
		}
		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}

	componentDidMount() {
		this._isMounted = true
		this.fetchCounterparties()
		if (this.props.match.params.id) {
			this.fetchTrade(parseInt(this.props.match.params.id))
		} else {
			this.loading = false
		}
	}

	componentWillUnmount() {
		this._isMounted = false
	}

	fetchCounterparties = async () => {
		const response = await ApiService.getCounterparties()
		if (this._isMounted) this.setState({ counterparties: response })
	}

	fetchTrade = async (id) => {
		ApiService.getTradeById(id).then(
			response => {
				response.date = moment(response.date).format('YYYY-MM-DD')
				this.loading = false
				if (this._isMounted) this.setState({ ...response })
			},
			reject => {
				window.alert(reject)
				this.loading = false
				this.props.history.push('/')
			})
	}

	handleChange(event) {
		const name = event.target.name
		const value = event.target.value
		this.setState({
			[name]: value
		})
	}

	handleSubmit(event) {
		const formValues = { ...this.state }
		formValues.counterpartyId = parseInt(formValues.counterpartyId)
		formValues.date = moment(formValues.date).toISOString()
		if (this.props.match.params.id) {
			ApiService.editTrade(formValues).then(response => {
				window.alert('Trade successfully edited!')
				this.props.history.push('/')
			})
		} else {
			ApiService.createTrade(formValues).then(response => {
				window.alert('Trade successfully created!')
				this.props.history.push('/')
			})
		}
		event.preventDefault()
	}

	render() {
		const loading = this.loading
		const options = []
		this.state.counterparties.forEach((counterparty) => {
			options.push(
				<option key={counterparty.id} value={counterparty.id}>{counterparty.name}</option>
			)
		})
		return (
			<div className="Form">
				<h1 className="Form-title">Form</h1>
				{loading && (
					<p>Loading...</p>
				)}
				{!loading && (
					<form onSubmit={this.handleSubmit}>
						<div className="Form-row">
							<label>Counterparty</label>
							<select name="counterpartyId" value={this.state.counterpartyId} onChange={this.handleChange} required>
								<option value="">Select a counterparty</option>
								{options}
							</select>
						</div>
						<div className="Form-row">
							<label>Product</label>
							<input name="product" type="text" placeholder="required" value={this.state.product} onChange={this.handleChange} required />
						</div>
						<div className="Form-row">
							<label>Quantity</label>
							<input name="quantity" type="number" placeholder="required" step="1" min="0" value={this.state.quantity} onChange={this.handleChange} required />
						</div>
						<div className="Form-row">
							<label>Price</label>
							<input name="price" type="number" placeholder="required" step="0.01" min="0" value={this.state.price} onChange={this.handleChange} required />
						</div>
						<div className="Form-row">
							<label>Date</label>
							<input name="date" type="date" placeholder="required" min={moment().format('YYYY-MM-DD')} value={this.state.date} onChange={this.handleChange} required />
						</div>
						<div className="Form-row">
							<label>Direction</label>
							<input type="radio" name="direction" value="Buy" checked={this.state.direction === 'Buy'} onChange={this.handleChange} /><span>Buy</span>
							<input type="radio" name="direction" value="Sell" checked={this.state.direction === 'Sell'} onChange={this.handleChange} /><span>Sell</span>
						</div>
						<button className="Form-submit" type="submit">Submit</button>
					</form>
				)}
			</div>
		)
	}
}
export default Form