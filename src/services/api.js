import { counterparties } from '../mock/counterparties'
import { trades } from '../mock/trades'

let Counterparties = counterparties
let Trades = trades

const ApiService = {
	getTrades: async () => {
		return new Promise(resolve => setTimeout(() => {
			resolve(Trades)
		}, 1500))
	},
	getCounterparties: async () => {
		return new Promise(resolve => setTimeout(() => {
			resolve(Counterparties)
		}, 1500))
	},
	getTradeById: async (id) => {
		const trade = Trades.find(trade => trade.id === id)
		if(trade !== undefined) {
			return new Promise(resolve => setTimeout(() => {
				resolve(trade)
			}, 1500))
		} else {
			return new Promise((promise, reject) => setTimeout(() => {
				reject(new Error('Could not find this trade'))
			}, 1500))
		}
	},
	createTrade: async (trade) => {
		let id = Trades.reduce(
			(max, trade) => (trade.id > max ? trade.id : max),
			Trades[0].id
		) + 1
		Trades.push({id: id, ...trade})
		return new Promise(resolve => setTimeout(() => {
			resolve(Trades)
		}, 1500))
	},
	editTrade: async (trade) => {
		const index = Trades.findIndex((i) => i.id === trade.id)
		Trades[index] = trade
		return new Promise(resolve => setTimeout(() => {
			resolve(Trades)
		}, 1500))
	},
	deleteTrade: async (id) => {
		const index = Trades.findIndex((i) => i.id === id)
		if(index !== -1) Trades.splice(Trades.findIndex((i) => i.id === id), 1)
		return new Promise(resolve => setTimeout(() => {
			resolve(Trades)
		}, 1500))
	}
}

export default ApiService